var indexSectionsWithContent =
{
  0: "#_abcdefghilmnoprstuw~",
  1: "abcfm",
  2: "a",
  3: "abcemrst",
  4: "abcdefghimnoprstw~",
  5: "_bcdgimprstu",
  6: "cmt",
  7: "acfhlmnps",
  8: "bo",
  9: "_a",
  10: "#a"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "related",
  9: "defines",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Friends",
  9: "Macros",
  10: "Pages"
};

