var searchData=
[
  ['savetofile_208',['saveToFile',['../classaruco_1_1BoardConfiguration.html#a2b17fca95eab5bf49b7c804a0117c8b8',1,'aruco::BoardConfiguration::saveToFile(string sfile)'],['../classaruco_1_1BoardConfiguration.html#a14ddb3e703e54030615c062b130bc569',1,'aruco::BoardConfiguration::saveToFile(cv::FileStorage &amp;fs)'],['../classaruco_1_1Board.html#a96568f81af7d25f6d518b6480ec43e3a',1,'aruco::Board::saveToFile()'],['../classaruco_1_1CameraParameters.html#a0fb7e36dcc255b6d8f6cb191bf5bb635',1,'aruco::CameraParameters::saveToFile()']]],
  ['setcornerrefinementmethod_209',['setCornerRefinementMethod',['../classaruco_1_1MarkerDetector.html#ae46e37f9795c2d95084fab9f46a36a7a',1,'aruco::MarkerDetector']]],
  ['setdesiredspeed_210',['setDesiredSpeed',['../classaruco_1_1MarkerDetector.html#a334888d444947beff4b92b7c37cf4a6a',1,'aruco::MarkerDetector']]],
  ['setmakerdetectorfunction_211',['setMakerDetectorFunction',['../classaruco_1_1MarkerDetector.html#a016caa551b0d14ca74913fa2036d0b5c',1,'aruco::MarkerDetector']]],
  ['setminmaxsize_212',['setMinMaxSize',['../classaruco_1_1MarkerDetector.html#a759698a524bb93ff3dd2a30dcb7e3d87',1,'aruco::MarkerDetector']]],
  ['setparams_213',['setParams',['../classaruco_1_1CameraParameters.html#af51ad02ac8a968ed20161baa18ca6435',1,'aruco::CameraParameters::setParams()'],['../classaruco_1_1BoardDetector.html#a87c75f63b4aa80d7f5c3afbddc7bface',1,'aruco::BoardDetector::setParams(const BoardConfiguration &amp;bc, const CameraParameters &amp;cp, float markerSizeMeters=-1)'],['../classaruco_1_1BoardDetector.html#ab98284bf35bbfc1c9af1b49558dacfcb',1,'aruco::BoardDetector::setParams(const BoardConfiguration &amp;bc)']]],
  ['setpointintoimage_214',['setPointIntoImage',['../namespacearuco.html#ae623f3002f10102bf0a4ce7865dd7c24',1,'aruco::setPointIntoImage(cv::Point &amp;p, cv::Size s)'],['../namespacearuco.html#a58f14fa6c21f19c308b6ca0124e9a8e2',1,'aruco::setPointIntoImage(cv::Point2f &amp;p, cv::Size s)']]],
  ['setthresholdmethod_215',['setThresholdMethod',['../classaruco_1_1MarkerDetector.html#af9f4d48cb107559f29a5c594ef6260b7',1,'aruco::MarkerDetector']]],
  ['setthresholdparams_216',['setThresholdParams',['../classaruco_1_1MarkerDetector.html#ae62e6b62bc82793d640c9c04ff00335c',1,'aruco::MarkerDetector']]],
  ['setyperpendicular_217',['setYPerpendicular',['../classaruco_1_1BoardDetector.html#a996922178edeecd39d80f65b8380001e',1,'aruco::BoardDetector']]],
  ['simple_5fsingle_2ecpp_218',['simple_single.cpp',['../simple__single_8cpp.html',1,'']]],
  ['simple_5fsingle_2ehpp_219',['simple_single.hpp',['../simple__single_8hpp.html',1,'']]],
  ['simple_5fsingle_5fnode_2ecpp_220',['simple_single_node.cpp',['../simple__single__node_8cpp.html',1,'']]],
  ['ssize_221',['ssize',['../classaruco_1_1Marker.html#ac5df65736cc539e78d6620307739b5b1',1,'aruco::Marker']]],
  ['subpix_222',['SUBPIX',['../classaruco_1_1MarkerDetector.html#ace0069f60aa5cef25a455be1e3d66dc1a6a41c357297ab089899bad573f91cc58',1,'aruco::MarkerDetector']]]
];
